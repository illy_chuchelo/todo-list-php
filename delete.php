<?php

require_once 'app/db.php';

if(isset($_GET['as'], $_GET['item'])){

    $as   = $_GET['as'];
    $item = $_GET['item'];

    switch($as){

        case 'delete':
            $deleteQuery = $db->prepare("
                      
                DELETE FROM items 
                WHERE id = :item
                AND user = :user
            ");

            $deleteQuery->execute([
                'item' => $item,
                'user' => $_SESSION['user_id']
            ]);
            break;
    }
}

header('Location: index.php');